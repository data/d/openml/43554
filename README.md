# OpenML dataset: Global-School-Closures-for-COVID-19

https://www.openml.org/d/43554

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The number of children, youth and adults not attending schools or universities because of COVID-19 is soaring. Governments all around the world have closed educational institutions in an attempt to contain the global pandemic.
According to UNESCO monitoring, over 100 countries have implemented nationwide closures, impacting over half of worlds student population. Several other countries have implemented localized school closures and, should these closures become nationwide, millions of additional learners will experience education disruption.
This data is compiled by the UNESCO and distributed by HDX.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43554) of an [OpenML dataset](https://www.openml.org/d/43554). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43554/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43554/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43554/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

